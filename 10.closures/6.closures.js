
// Problem
const array = [1, 2, 3, 4];

for (var i = 0; i < array.length; i++) {
    setTimeout(function () {
        console.log('Problem: I am at index ' + array[i]);
    }, 3000);
}

// Solution1 with let
console.log("===solution 1=====")
for (let i = 0; i < array.length; i++) {
    setTimeout(function () {
        console.log('Solution 1:  I am at index ' + array[i]);
    }, 3000);
}

// Solution2 with closure
console.log("===solution 2=====")
for (let i = 0; i < array.length; i++) {
    (function (closureI) {
        setTimeout(function () {
            console.log('Solution 2: I am at index ' + array[closureI]);
        }, 3000);
    })(i);
}