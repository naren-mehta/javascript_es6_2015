



let view;


// function initialize() {
//     view = 'Sunsine';
//     console.log('view has been set')
//     called++;
// }

// initialize();
// initialize();
// initialize();
// initialize();
// console.log(view+"==="+called);


function initialize() {
    let called = 0;

    return function () {
        if (called > 0) {
            return;
        } else {
            view = 'Sunsine';
            console.log('view has been set')
            called++;
        }
    }
}

// const startOnce = initialize();
// startOnce();
// startOnce();
// startOnce();
// startOnce();
// startOnce();
// startOnce();
// startOnce();

// console.log(view);