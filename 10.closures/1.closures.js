
function a(){
    let grandpa='grandpa';
    return function b(){
        let father='father';
        return function c(){
            let son='son';
            return `${grandpa} > ${father} > ${son}`
        }
    }
}

console.log(a()()());


// Other way

const boo = (first) => (second) => (third) => `${first} ${second} ${third}`;
boo('Grandpa')('Father')('Son');


//===================
const booGrandPa = boo('grandpa');
const booFather = booGrandPa('father');
const booSon= booFather('son');

console.log("=========="+booSon);