// Memory efficient

function heavyDuty(index){
    const bigArray= new Array(7000).fill(':)');
    console.log('Created!');

    return bigArray[index];
}

console.log(heavyDuty(688));
console.log(heavyDuty(688));
console.log(heavyDuty(688));


function heavyDuty2(){
    const bigArray= new Array(7000).fill(':)');
    console.log('Created again!');
    return function(index){
        return bigArray[index];
    }
}

const getHeavyDety2 = heavyDuty2();
console.log(getHeavyDety2(777));
console.log(getHeavyDety2(888));
console.log(getHeavyDety2(999));



// Encapsulation