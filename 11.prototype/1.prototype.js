let dragon = {
    name: 'Auo',
    fire:true,
    fight(){
        return 5;
    },
    sing(){
        return `I am ${this.name}, the breather of fire`
    }
}

let lizard = {
    name:'kiki',
    fight(){
        return 1;
    }
}

// One way to copy methods of one object to another
// const singLizard= dragon.sing.bind(lizard);
// console.log(singLizard());

// 2nd way - protottype inheritance

lizard.__proto__ = dragon;
console.log(lizard.fight());
console.log(lizard.sing());
console.log(lizard.fire);

console.log(' dragon isPrototypeOf lizard ',dragon.isPrototypeOf(lizard));
console.log('lizard isPrototypeOf dragon ',lizard.isPrototypeOf(dragon));
