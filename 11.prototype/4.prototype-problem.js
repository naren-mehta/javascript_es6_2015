
var obj={
    date:new Date(),
    lastYear(){
        return new Date(new Date().setFullYear(new Date().getFullYear()-1));
    }
}

var newDate = new Date();
newDate.__proto__= obj;


console.log(newDate.lastYear());


// Better way

Date.prototype.lastYear = function(){
    return this.getFullYear()-1;
}

console.log("new ", new Date().lastYear());


// 

Array.prototype.map= function(){
    let arr=[];
    for(let i=0;i<this.length;i++){
        arr.push(this[i]+"**");
    }

    return arr;
}

console.log([1,2,3].map())