
// 1st way of function
function one() {
    return 1;
}

console.log(one());

// 2nd way of function

var obj = {
    two: function () {
        return 2;
    }
}

console.log(obj.two());

// 3rd way of function

var obj1 = {
    three() {
        return 3;
    }
}

console.log(obj1.three());

// 4th way of function
function four() {
    return 4;
}

console.log(four.call());

// 5th way of function == called 'Function constructor'

const fifth = new Function("return 5");

console.log(fifth());

// 5.2th way of function == called 'Function constructor'

const sixth = new Function("num","return num");

console.log(sixth(100));