

// functions are first class citizens in js
//1

var stuff = function(){
    console.log("====stuff====");
}
stuff();

//2
function a(fn){
    fn();
} 

a(function(){console.log("====passing a function to a method-===")});

//3

function b(){
    return function c(){console.log(' c bye ')}
}

console.log(b());
b()();