// Bad way

// function letAdamLogin(){
//     let array =[];

//     for(let i=0;i<10000000;i++){
//         array.push(i);
//     }

//     return 'Access Granted to Adam';
// }


// function letEvaLogin(){
//     let array =[];

//     for(let i=0;i<10000000;i++){
//         array.push(i);
//     }

//     return 'Access Granted to eva';
// }

// console.log(letAdamLogin());
// console.log(letEvaLogin());

// better way======================================================================




// const getAccessTo = (user) =>{
//     return 'Access Granted to '+user
// }

// function letUserLogin(user){
//     let array =[];

//     for(let i=0;i<10000000;i++){
//         array.push(i);
//     }

//     return getAccessTo(user);
// }

// console.log(letUserLogin('Adam'));
// console.log(letUserLogin('Eva'));


// High Order function==============================================


const getAccessTo = (user) =>{
    return 'Access Granted to '+user
}

function authenticate(verify) {
    let array = [];

    for (let i = 0; i < verify; i++) {
        array.push(i);
    }

    return true;
}

function letPerson(person, fn) {
    if (person.level == 'admin') {
        fn(500000);
    } else if (person.level == 'user') {
        fn(100000);
    }else{
        fn(1000)
    }

    return getAccessTo(person.name);
}

console.log(letPerson({level:'user', name:'Tim'}, authenticate));
console.log(letPerson({level:'admin', name:'Eva'},authenticate));









