
// Closure
// const multiplyBy = function (num1){
//     return function (num2){
//         return num1*num2;
//     }
// }

// const multiplyByTwo = multiplyBy(2);
// const multiplyByFive = multiplyBy(5);


// console.log(multiplyByTwo(10));
// console.log(multiplyByFive(10));

// =================================================================
// in one line

const multiplyBy = (num1) => (num2) => num1*num2;

console.log(multiplyBy(10)(5))