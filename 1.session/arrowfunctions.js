

// Arrow function have lexical this variable

// ES5

// var box5={
//     color :'green',
//     position:1,
//     clickMe: function(){
//         var self= this;
//         console.log(this)
//         document.querySelector('.green').addEventListener('click', function(){

//             console.log(this)

//             var str= 'This is box number '+self.position +' and it is '+self.color;
//             alert(str);
//         })
//     }
// }

// box5.clickMe();

var box6={
    color :'green',
    position:1,
    clickMe: ()=>{
        var self= this;
        console.log(this)
        document.querySelector('.green').addEventListener('click', ()=>{

            console.log("======");
            console.log(this);

            var str= 'This is box number '+self.position +' and it is '+self.color;
            alert(str);
        })
    }
}

box6.clickMe();
