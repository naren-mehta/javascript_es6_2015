

function addFourAges(a, b, c, d){
    return a+b+c+d;
}

var sum1= addFourAges(10,20,30,40);
console.log(sum1);

// ES5

var ages=[20,21,32,43];
var sum2 = addFourAges.apply(null, ages);
console.log(sum2);