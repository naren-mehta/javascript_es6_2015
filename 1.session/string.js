

let firstName='Naren';
let lastName='Mehta';

const yearOfBirth = 1990;

function calcAge(year){
    return 2019-year;
}

// ES5
console.log('He is ' +firstName + ' '
+ lastName +'. He born in '+yearOfBirth +'. Today he is '+calcAge(yearOfBirth) +' years old.');

// ES6

console.log(`New String with back tick :  He is ${firstName} ${lastName} . He born in ${yearOfBirth} . `
+`Today he is ${calcAge(yearOfBirth)} years old`);


const n= `${firstName} ${lastName}`;

console.log(n.startsWith('N'));
console.log(n.endsWith('a'));
console.log(n.includes('are'));

console.log((firstName + ' ').repeat(5));

